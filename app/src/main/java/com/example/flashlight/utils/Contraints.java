package com.example.flashlight.utils;

import android.app.ActivityManager;
import android.content.Context;

public class Contraints {
    // check service is running?
    public static boolean isMyServiceRunning(final Class<?> serviceClass,
                                             final Context context) {
        ActivityManager manager = (ActivityManager) context.
                getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(
                Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}

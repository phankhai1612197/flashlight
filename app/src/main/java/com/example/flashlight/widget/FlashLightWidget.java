package com.example.flashlight.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.example.flashlight.R;
import com.example.flashlight.model.FlashLightService;

public class FlashLightWidget extends AppWidgetProvider {

    private static final int REQUEST_CODE = 321;


    private static final String ACTION_START = "start";

    private static final String ACTION_STOP = "stop";

    private static final String ACTION_UPDATE = "update";

    private static boolean isFlash = false;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int widgetId : appWidgetIds) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                    R.layout.layout_widget);
            remoteViews.setOnClickPendingIntent(R.id.img_light_widget, getService(context,
                    FlashLightService.class, ACTION_START));

//            remoteViews.setOnClickPendingIntent(R.id.img_light_widget, getBroadCast(context,
//                    ACTION_UPDATE));
            appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);
        }
    }

    private PendingIntent getBroadCast(final Context context, final String action) {
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, REQUEST_CODE, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private PendingIntent getService(final Context context, Class T, final String action) {
        Intent intent = new Intent(context, T);
        intent.setAction(action);
        PendingIntent pendingIntent = PendingIntent.getService(context, REQUEST_CODE, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        return pendingIntent;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION_UPDATE)) {

        }
        super.onReceive(context, intent);
    }
}

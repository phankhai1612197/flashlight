package com.example.flashlight.model;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.example.flashlight.R;
import com.example.flashlight.controller.MainActivity;
import com.example.flashlight.widget.FlashLightWidget;

public class FlashLightService extends Service {
    private static final String TAG = FlashLightService.class.getName();

    private static final String ACTION_START = "start";

    private Camera mCamera;

    private Camera.Parameters mParams;

    private MediaPlayer mMediaPlayer;

    private static boolean isFlash = false;

    private static final String ACTION_START_ACTIVITY = "start_activity";

    /**
     *
     */
    @Override
    public void onCreate() {
        getCamera();
        super.onCreate();
    }

    // getting mCamera parameters
    private void getCamera() {
        if (mCamera == null) {
            try {
                mCamera = Camera.open();
                mParams = mCamera.getParameters();
            } catch (RuntimeException e) {
                Log.e(TAG, e.getMessage().toString());
            }
        }
    }

    /*
     * Turning On flash
     */
    public void turnFlash(final boolean isFlashOn) {
        Log.d(TAG, "turnFlash: " + isFlashOn);
        if (mCamera != null || mParams != null) {
            if (!isFlashOn) {
                // play sound
                playSound(isFlashOn);

                mParams = mCamera.getParameters();
                mParams.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                mCamera.setParameters(mParams);
                mCamera.stopPreview();
            } else {
                // play sound
                playSound(isFlashOn);

                mParams = mCamera.getParameters();
                mParams.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                mCamera.setParameters(mParams);
                mCamera.startPreview();
            }
        }
    }

    /*
     * Playing sound will play button toggle sound on flash on / off
     */
    private void playSound(final boolean isFlashOn) {
        if (isFlashOn) {
            mMediaPlayer = MediaPlayer.create(getApplicationContext(),
                    R.raw.light_switch_off);
        } else {
            mMediaPlayer = MediaPlayer.create(getApplicationContext(),
                    R.raw.light_switch_on);
        }
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.release();
            }
        });
        mMediaPlayer.start();
    }

    /**
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getAction().equals(ACTION_START)) {
            Context context = this;
            receiveFromWidget(context);
        } else {
            if (intent.getAction().equals(ACTION_START_ACTIVITY)) {
                startForeground(101, updateNotification());
                turnFlash(true);
            }
        }
        return START_STICKY;
    }

    private void receiveFromWidget(final Context context) {
        //startForeground with widget from homeScreen
        startForeground(101, updateNotification());

        AppWidgetManager appWidgetManager = AppWidgetManager.
                getInstance(context);
        ComponentName componentName = new ComponentName(context
                .getPackageName(), FlashLightWidget.class.getName());

        int appWidgetIds[] = appWidgetManager.getAppWidgetIds(componentName);

        isFlash = isFlash ? false : true;

        RemoteViews remoteViews = new RemoteViews(componentName.
                getPackageName(), R.layout.layout_widget);

        remoteViews.setImageViewResource(R.id.img_light_widget,
                isFlash ? R.drawable.ic_light_widget_on : R.drawable.ic_light_widget_off);

        appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);

        turnFlash(isFlash);
        if (!isFlash) {
            stopForeground(true);
            stopSelf();
        }

    }

    /**
     * @return
     */
    private Notification updateNotification() {

        Context context = getApplicationContext();

        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                0, new Intent(context, MainActivity.class),
                PendingIntent.FLAG_CANCEL_CURRENT);

        // Flag indicating that if the described PendingIntent already exists and
        // the current one should be canceled before generating a new one.
        NotificationManager manager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder = null;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            String CHANNEL_ID = "khaipm";

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "KhaiPM",
                    NotificationManager.IMPORTANCE_HIGH);

            channel.setDescription("Description for KhaiPM");

            manager.createNotificationChannel(channel);

            builder = new NotificationCompat.Builder(this, CHANNEL_ID);

        } else {
            builder = new NotificationCompat.Builder(context, "");
        }

        builder.setContentIntent(pendingIntent).setContentIntent(pendingIntent).
                setContentTitle(getResources().getString(R.string.press_to_turn_off)).
                setSmallIcon(R.drawable.ic_light_widget_off).setOngoing(true);
        return builder.build();
    }

    /**
     *
     */
    @Override
    public void onDestroy() {
        // on stop release the mCamera
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
        if (mParams != null) {
            mParams = null;
        }
        super.onDestroy();
    }

    /**
     * @param intent
     * @return
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

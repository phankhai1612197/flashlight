package com.example.flashlight.controller;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.flashlight.R;
import com.example.flashlight.dialog.DialogNotification;
import com.example.flashlight.model.FlashLightService;
import com.example.flashlight.utils.Contraints;
import com.example.flashlight.utils.PermissionUtil;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        DialogNotification.OnAceptListener {

    private static final String TAG = MainActivity.class.getName();

    private ImageView mImgLight;

    private static final int REQUEST_CODE_PERMISSION = 321;

    private static final String PERMISSION_CAMERA = "android.permission.CAMERA";

    private static final String ACTION_START_ACTIVITY = "start_activity";

    private static final String ACTION_STOP_ACTIVITY = "stop_activity";

    private boolean isFlash = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!PermissionUtil.hasPermissions(this, PERMISSION_CAMERA)) {
            PermissionUtil.requestPermission(MainActivity.this, new String[]{PERMISSION_CAMERA}, REQUEST_CODE_PERMISSION);
        } else {
            //initView
            initView();
            //registerListener
            registerListener();
            //check exist of flashlight on device
            if (isCheckFlashLight()) {

            } else {
                showDialogNotification();
            }

            if (Contraints.isMyServiceRunning(FlashLightService.class, this)) {
                mImgLight.setImageResource(R.drawable.ic_light_active);
                isFlash = true;
            } else {
                mImgLight.setImageResource(R.drawable.ic_light_deactive);
                isFlash = false;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //initView
                initView();
                //registerListener
                registerListener();
                //check exist of flashlight on device
                if (isCheckFlashLight()) {
                } else {
                    showDialogNotification();
                }
            } else {
                Toast.makeText(this, "" + getResources().getString(R.string.permission_camera), Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void initView() {
        mImgLight = findViewById(R.id.img_light);
    }

    private void registerListener() {
        mImgLight.setOnClickListener(this);
    }

    /**
     * @param view
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_light:
                isFlash = isFlash ? false : true;
                mImgLight.setImageResource(isFlash ? R.drawable.ic_light_active :
                        R.drawable.ic_light_deactive);
                if (isFlash) {
                    Intent intent = new Intent(this, FlashLightService.class);
                    intent.setAction(ACTION_START_ACTIVITY);
                    startService(intent);
                } else {
                    Intent intent = new Intent(this, FlashLightService.class);
                    intent.setAction(ACTION_STOP_ACTIVITY);
                    stopService(intent);
                }
                break;
        }
    }

    private void showDialogNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            DialogNotification dialogNotification =
                    new DialogNotification(MainActivity.this);
            dialogNotification.create();
            dialogNotification.setTextNotification(getResources().getString(R.string.notification));
            dialogNotification.show();
        }
    }

    private boolean isCheckFlashLight() {
        return getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    @Override
    public void setonAceptListener() {
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }


}

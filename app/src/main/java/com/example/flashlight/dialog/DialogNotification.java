package com.example.flashlight.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.example.flashlight.R;

public class DialogNotification extends AlertDialog implements View.OnClickListener {

    private Context mContext;

    private TextView mTvNotification;

    private Button mBtnNo;

    private Button mBtnYes;

    private OnAceptListener mOnAceptListener;

    public DialogNotification(@NonNull Context context) {
        super(context);
        this.mContext = context;
    }

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_dialog_notification);

        setCancelable(false);

        initView();

        registerListener();

        if (mOnAceptListener == null) {
            mOnAceptListener = (OnAceptListener) mContext;
        }
    }

    /**
     * @param onAceptListener
     */
    public void setOnAceptListener(OnAceptListener onAceptListener) {
        this.mOnAceptListener = onAceptListener;
    }

    private void initView() {
        mTvNotification = findViewById(R.id.tv_dialog_notification);
        mBtnNo = findViewById(R.id.btn_dialog_no);
        mBtnYes = findViewById(R.id.btn_dialog_yes);

    }

    private void registerListener() {
        mBtnYes.setOnClickListener(this);
        mBtnNo.setOnClickListener(this);
    }

    public TextView getTvNotification() {
        return mTvNotification;
    }

    public Button getBtnNo() {
        return mBtnNo;
    }

    public Button getBtnYes() {
        return mBtnYes;
    }

    public void setTextNotification(final String notification) {
        this.mTvNotification.setText(notification);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_dialog_no:
                break;
            case R.id.btn_dialog_yes:
                if (mOnAceptListener != null) {
                    mOnAceptListener.setonAceptListener();
                }
                break;
        }
        dismiss();
    }

    public interface OnAceptListener {
        void setonAceptListener();
    }
}
